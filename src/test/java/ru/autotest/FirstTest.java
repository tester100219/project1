package ru.autotest;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.Assert.assertEquals;

public class FirstTest {
    private static WebDriver driver;

    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://habr.com/ru/");
        assertEquals("Лучшие публикации за сутки / Хабр", driver.getTitle());
    }

    @Test
    public void loginHabr(){
        WebElement login = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("login")));
        login.click();

        WebElement btn_email = driver.findElement(By.id("email_field"));
        btn_email.sendKeys("tester100219@inbox.ru");
        WebElement btn_password = driver.findElement(By.id("password_field"));
        btn_password.sendKeys("12345678.w");

        WebElement btn_enter = driver.findElement(By.name("go"));
        btn_enter.submit();
    }

    //@Test
    public void getBest(){
        WebElement post =
                driver.findElement(By.xpath("/html/body/div[3]/div[3]/div/section/div[1]/div[2]/div[2]/ul/li[4]/a"));
        post.click();
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }
}
